defmodule Smaug.SearchDsl.DslClient do
  @moduledoc """
  Behaviour for search DSL client wrappers
  """

  @doc """
  Handler for converting the search params to the
  client-specifc format and executing the request
  """
  @callback call(map(), Brand.t()) :: {:ok, map()} | {:error, map()}

  @doc """
  Handles various response formats from the client server and 
  routes appropriately 
  """
  @callback handle_response(tuple(), String.t() | nil) :: {:ok, map()} | {:error, map()}

  @doc """
  Handler for converting the response from the external client
  into a generic structure 
  """
  @callback transform_response(String.t() | nil, map()) :: {:ok, map()} | {:error, map()}
end
