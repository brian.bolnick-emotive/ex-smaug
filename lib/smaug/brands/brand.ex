defmodule Smaug.Brands.Brand do
  use Ecto.Schema
  import Ecto.Changeset

  alias Smaug.BrandIntegrations.BrandIntegration

  @fields [
    :name,
    :company_name
  ]

  schema "brands" do
    field :name, :string
    field :company_name, :string

    has_many(:brand_integrations, BrandIntegration)
    timestamps()
  end

  @doc false
  def changeset(brand, attrs) do
    brand
    |> cast(attrs, @fields)
    |> validate_required(@fields)
  end
end
