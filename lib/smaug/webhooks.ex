defmodule Smaug.Webhooks do
  @moduledoc """
  Entrypoint for the webhooks context
  """
  require Logger

  alias Smaug.Brands
  alias Brands.Brand

  def handle_stripe_payload("account.updated", %{
        "data" => %{"object" => data},
        "livemode" => is_live
      }) do
    branding = get_in(data, ["settings", "branding"]) || nil
    business_name = get_in(data, ["business_profile", "name"]) || nil
    platform = get_in(data, ["metadata", "platform"]) || "big_commerce"
    brand_id = get_in(data, ["metadata", "brand_id"]) || nil

    brand_params = %{
      name: business_name,
      platform: platform,
      sensus_brand_id: brand_id,
      branding_config: branding,
      is_test: !is_live
    }

    case Brands.create_brand(brand_params) do
      {:ok, %Brand{} = brand} ->
        Logger.info("[Webhooks] successfully created brand with id: #{brand.id}")
        :ok

      {:error, err} ->
        Logger.warn("[Webhooks] failed to create brand with error: #{inspect(err)}")
        :error
    end
  end

  def handle_stripe_payload("account.updated", bad_payload) do
    Logger.warn("[Webhooks] bad payload for account.updated: #{inspect(bad_payload)}")
    :ok
  end
end
