defmodule Smaug.BrandIntegrations.BrandIntegration do
  use Ecto.Schema
  import Ecto.Changeset

  alias Smaug.Brands.Brand
  alias Smaug.BrandIntegrations.BrandPlatform

  @fields [
    :access_token,
    :brand_id,
    :platform
  ]

  schema "brand_integrations" do
    field :access_token, :string
    field :platform, BrandPlatform

    belongs_to(:brand, Brand)

    timestamps()
  end

  @doc false
  def changeset(brand, attrs) do
    brand
    |> cast(attrs, @fields)
    |> validate_required(@fields)
  end
end
