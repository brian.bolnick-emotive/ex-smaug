defmodule SmaugWeb.Router do
  use SmaugWeb, :router

  alias SmaugWeb.WebhooksController

  pipeline :webhooks do
    plug :accepts, ["json"]
  end

  pipeline :graphql do
    plug :accepts, ["json"]

    # Add authentication and other helper plugs here
  end

  scope "/graphql" do
    pipe_through(:graphql)

    post("/", Absinthe.Plug, schema: SmaugWeb.Schema)
  end

  scope "/webhooks" do
    pipe_through(:webhooks)

    post("/stripe", WebhooksController, :stripe)
  end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through [:fetch_session, :protect_from_forgery]
      live_dashboard "/dashboard", metrics: SmaugWeb.Telemetry
    end
  end
end
