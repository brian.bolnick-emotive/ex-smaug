defmodule SmaugWeb.Schema.Brands.Types do
  @moduledoc """
  Types for brand schema
  """
  use Absinthe.Schema.Notation

  enum(:brand_platform,
    values: [
      :shopify,
      :big_commerce,
      :woo_commerce,
      :magento
    ]
  )

  object :brand do
    field(:id, :id)
    field(:name, :string)
    field(:platform, :brand_platform)
    field(:shop_url, :string)
    field(:shop_token, :string)
    field(:shop_meta, :string)
    field(:inserted_at, :string)
  end
end
