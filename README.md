# Smaug

This is a demo and rapid prototype for proxying product search requests to multiple e-commerce platform providers (such as Shopify and BigCommerce). This is for demonstration purposes only.

## Project Setup:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Testing the prototype
The end result of this project should allow for receiving a single generic query (the UI client) and respond back with a single generic response, regardless of the connected e-commerce platform. In order to effectively test this, you will need to create multiple brands with varying `platform_id`s in the local database. Currently, this only supports BigCommerce and Shopify.

To QA manually, try running the following query against a configured store instance:

```
POST localhost:4000/graphql #currently unauthenticated

query productSearch($name: String, $brandId: ID!) {
  searchProductsQuery(name: $name, brandId:$brandId) {
    data {
      id
      name
      type
      description
      inventoryLevel
      price
      productUrl
      imageUrl
    }
    size
  }
}
```

For now, the `brandId` is a required parameter as there is no authentication in place. This is strictly for simulating the "current user/brand."

The `name` variable will be translated to the appropriate field for the e-commerce client (ie `name` for BigCommerce or `title` for shopify). Future filters would be added in a similar manner.

If the simulation is successful, you should get back a full payload of the correct products in the same shape, regardless of the inventory platform.

## Other useful queries for testing:

Get all brands
```
query allBrandsQuery{
  allBrandsQuery{
      id
    name
    platform
    shopUrl
    shopToken
    shopMeta
  }
}
```

Get a single Brand
```
query brandQuery($id: ID!) {
  brandQuery(id:$id){
     id
    name
    platform
    shopUrl
    shopToken
    shopMeta
    insertedAt
  }
}
```

Create a brand
```
mutation createBrand(
  $name: String!
  $platform: String!
  $shopUrl: String
  $shopToken: String
  $shopMeta: String
) {
  createBrand(
    name:$name
    platform: $platform
    shopUrl:$shopUrl
    shopToken: $shopToken
    shopMeta: $shopMeta
  ) {
    id
    name
    platform
    shopUrl
    shopToken
    shopMeta
  }
}
```
