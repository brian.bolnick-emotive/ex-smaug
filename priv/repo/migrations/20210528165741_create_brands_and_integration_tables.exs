defmodule Smaug.Repo.Migrations.CreateBrandsTable do
  use Ecto.Migration

  alias Smaug.BrandIntegrations.BrandPlatform

  def up do
    BrandPlatform.create_type()

    create table(:brands) do
      add(:name, :string, null: false)
      add(:company_name, :string, null: false)

      timestamps()
    end

    create table(:brand_integrations) do
      add(:access_token, :string, null: false)
      add(:platform, BrandPlatform.type(), null: false)
      add(:brand_id, references(:brands, on_delete: :delete_all), null: false)

      timestamps()
    end

    create(index(:brand_integrations, [:brand_id]))
  end

  def down do
    drop(index(:brand_integrations, [:brand_id]))

    drop(table(:brands))
    drop(table(:brand_integrations))

    BrandPlatform.drop_type()
  end
end
